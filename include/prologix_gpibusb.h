// prologix_gpibusb.h
#ifndef INCLUDED_PROLOGIX_GPIBUSB
#define INCLUDED_PROLOGIX_GPIBUSB

#include"serial_interface.h"

//#define DEBUG

class SerialCom;

class prologix_gpibusb : public serial_interface
{
 public:
  prologix_gpibusb();
  ~prologix_gpibusb();

  int initialize();
  int finalize();

  int set_address(int address);

  int write(std::string command);
  std::string read(std::string &buffer);

  int make_listener();
  int make_talker();

 private:
  SerialCom *m_serial;

};

#endif
