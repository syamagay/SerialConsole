// power_supply.h
#ifndef INCLUDED_POWER_SUPPLY
#define INCLUDED_POWER_SUPPLY

class serial_interface;

class power_supply
{
 public:
  power_supply(){};
  virtual ~power_supply(){};

  virtual int initialize(serial_interface *si = nullptr) = 0;
  virtual int finalize(serial_interface *si = nullptr) = 0;
  virtual int reset(serial_interface *si) = 0;
  virtual int configure(serial_interface *si) = 0;
  virtual int power_on(serial_interface *si) = 0;
  virtual int power_off(serial_interface *si) = 0;
  virtual int config_voltage(serial_interface *si) = 0;
  virtual int config_compliance(serial_interface *si) = 0;
  virtual void read_voltage_and_current(serial_interface *si, double &voltage, double &current) = 0;
  virtual double read_voltage(serial_interface *si) = 0;
  virtual double read_current(serial_interface *si) = 0;

  virtual void set_operating_channel(int channel){m_operating_channel = channel;}; //needed for multi-channel devices;
  int get_operating_channel(){return m_operating_channel;};

  virtual bool is_on(serial_interface *si) = 0;
  virtual bool is_off(serial_interface *si) = 0;

  virtual int voltage_sweep(serial_interface *si){};

  int get_address(){return m_address;};
  double get_voltage(int channel=0){return m_voltage[channel];};
  double get_compliance(int channel=0){return m_compliance[channel];};

  virtual void set_address(int address){m_address = address;}; //needed for GPIB devices;
  virtual void set_voltage(double voltage, int channel=0){m_voltage[channel] = voltage;};
  virtual void set_compliance(double compliance, int channel=0){m_compliance[channel] = compliance;};

  void set_sweep_steps(int sweep_steps){m_sweep_steps = sweep_steps;};
  void set_sweep_sleep_in_ms(int sweep_sleep_in_ms){m_sweep_sleep_in_ms = sweep_sleep_in_ms;};

  virtual std::string get_device_type(serial_interface *si = nullptr){return "UNDEFINED";};
  virtual std::string get_ofile_name(){return "data/IV_"+this->get_device_type()+"_addr"+std::to_string(m_address)+".dat";};

  void set_enable(bool enable, int channel=0){m_enable[channel] = enable;};
  bool get_enable(int channel=0){return m_enable[channel];};

  void set_num_available_channel(int nchan=0){m_num_available_channel = nchan;};
  int get_num_available_channel(){return m_num_available_channel;};

 protected:
  int m_address = -1;
  std::string m_devicetype = "UNDEFINED";
  int m_operating_channel = 0;
  int m_num_available_channel = 0;
  bool m_enable[256] = {false};
  double m_voltage[256] = {0.};
  double m_compliance[256] = {0.};

  int m_sweep_steps = 50;
  int m_sweep_sleep_in_ms = 0;

};

#endif
