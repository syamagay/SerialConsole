// texio_pw_a_series.h
#ifndef INCLUDED_TEXIO_PW_A_SERIES
#define INCLUDED_TEXIO_PW_A_SERIES

#include"power_supply.h"

class texio_pw_a_series : public power_supply
{
 public:
  texio_pw_a_series(){};
  texio_pw_a_series(int address){m_address = address;};
  ~texio_pw_a_series(){};

  int initialize(serial_interface *si = nullptr);
  int finalize(serial_interface *si = nullptr);

  int reset(serial_interface *si);
  int configure(serial_interface *si);
  int power_on(serial_interface *si);
  int power_off(serial_interface *si);
  int config_voltage(serial_interface *si);
  int config_compliance(serial_interface *si);
  void set_operating_channel(int operating_channel);
  double read_voltage(serial_interface *si);
  double read_current(serial_interface *si);
  void read_voltage_and_current(serial_interface *si, double &voltage, double &current);

  int voltage_sweep(serial_interface *si);

  bool is_on(serial_interface *si);
  bool is_off(serial_interface *si);

  void set_sweep_steps(int sweep_steps){m_sweep_steps = sweep_steps;};
  void set_sweep_sleep_in_ms(int sweep_sleep_in_ms){m_sweep_sleep_in_ms = sweep_sleep_in_ms;};

  std::string get_device_type(serial_interface *si);

 private:

  std::string m_str_channel = "";

};

#endif
