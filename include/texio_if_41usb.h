// texio_if_41usb.h
#ifndef INCLUDED_TEXIO_IF_41USB
#define INCLUDED_TEXIO_IF_41USB

#include"serial_interface.h"
#include<termios.h>

//#define DEBUG

//Interface type
enum itype{usb, rs232};

class texio_if_41usb : public serial_interface
{
 public:
  texio_if_41usb();
  ~texio_if_41usb();

  int initialize();
  int finalize();

  int set_address(int address){m_address = address;};

  int write(std::string command);
  std::string read(std::string &buffer);

  int make_listener();
  int make_talker();

 private:

  int fd;
  struct termios oldtio, newtio;
  char* buf;

 protected:

};

#endif
