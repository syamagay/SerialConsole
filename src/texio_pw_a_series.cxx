#include<iostream>
#include<algorithm>
#include<chrono>
#include<thread>
#include<cmath>
#include<unistd.h>

#include"serial_interface.h"
#include"texio_pw_a_series.h"

int texio_pw_a_series::initialize(serial_interface *si)
{
  m_devicetype = this->get_device_type(si);
  return 0;
}

int texio_pw_a_series::finalize(serial_interface *si)
{
  return 0;
}

int texio_pw_a_series::reset(serial_interface *si)
{
  //  si->set_address(m_address);
  //  si->write("*RST\r\n");
  return 0;
}

int texio_pw_a_series::configure(serial_interface *si)
{
  si->set_address(m_address);
  //  si->make_listener();

  si->write("PR0");//Selecting preset 4

  std::string tmpstr;
  for(int ichan=0; ichan<this->get_num_available_channel(); ichan++){
    this->set_operating_channel(ichan);
    if(m_enable[ichan]){
      tmpstr += "O"+m_str_channel+"1,";//Enabling/disabling channels
      tmpstr += "V"+m_str_channel+std::to_string(m_voltage[ichan]).substr(0,5)+",";
      tmpstr += "A"+m_str_channel+std::to_string(m_compliance[ichan]).substr(0,5)+",";
    }else{
      tmpstr += "O"+m_str_channel+"0,";
      tmpstr += "V"+m_str_channel+"0000,";
      tmpstr += "A"+m_str_channel+"0000,";
    }
  }

  tmpstr = tmpstr.substr(0,tmpstr.length()-1);//Removing the last comma.
  si->write(tmpstr);

  //Display voltage/current
  for(int ichan=1; ichan<=this->get_num_available_channel(); ichan++){
    this->set_operating_channel(ichan);
    si->write("DS"+std::to_string(ichan+1));
    if(ichan!=this->get_num_available_channel())usleep(500000);
  }

  si->write("DS1");//Display channel 1 as default

  return 0;
}

int texio_pw_a_series::power_on(serial_interface *si)
{
  si->set_address(m_address);
  si->write("SW1");

  return 1;
}

int texio_pw_a_series::power_off(serial_interface *si)
{
  si->set_address(m_address);
  si->write("SW0");
  //  usleep(1000000);//Need to wait for a while...

  return 1;
}

int texio_pw_a_series::voltage_sweep(serial_interface *si)
{
  // not implemented for LV supply

  return 0;
};

int texio_pw_a_series::config_voltage(serial_interface *si)
{
  si->set_address(m_address);
  //  si->write(":SOURCE:VOLTAGE:LEVEL "+std::to_string(m_voltage)+"\r\n");
  return 0;
}

int texio_pw_a_series::config_compliance(serial_interface *si)
{
  si->set_address(m_address);
  //  si->write(":SENSE:CURRENT:PROTECTION "+std::to_string(m_compliance)+"\r\n");
  return 0;
}

void texio_pw_a_series::set_operating_channel(int operating_channel)
{
  if     (operating_channel==0) m_str_channel = "A";
  else if(operating_channel==1) m_str_channel = "B";
  else if(operating_channel==2) m_str_channel = "C";
  else if(operating_channel==3) m_str_channel = "D";
  m_operating_channel = operating_channel;
}

bool texio_pw_a_series::is_on(serial_interface *si)
{
  //
  return true;
}

bool texio_pw_a_series::is_off(serial_interface *si)
{
  return (not this->is_on(si));
}

double texio_pw_a_series::read_voltage(serial_interface *si)
{
  // std::string buffer;
  // si->set_address(m_address);
  // si->write(":READ?\r\n");
  // si->make_talker();
  // si->read(buffer);
  // double voltage = std::stod(buffer.substr(0,13));
  // si->make_listener();

  //  return voltage;
  return 0.;
}

double texio_pw_a_series::read_current(serial_interface *si)
{
  // std::string buffer;
  // si->set_address(m_address);
  // si->write(":READ?\r\n");
  // si->make_talker();
  // si->read(buffer);
  // double current = std::stod(buffer.substr(14,13));
  // si->make_listener();

  // return current;
  return 0;
}

void texio_pw_a_series::read_voltage_and_current(serial_interface *si, double &voltage, double &current)
{
  // std::string buffer;
  // si->set_address(m_address);
  // si->write(":READ?\r\n");
  // si->make_talker();
  // si->read(buffer);
  // voltage = std::stod(buffer.substr(0,13));
  // current = std::stod(buffer.substr(14,13));
  // si->make_listener();

  return;
}

std::string texio_pw_a_series::get_device_type(serial_interface *si)
{
  if(si == nullptr){
    return m_devicetype;
  }

  si->set_address(m_address);
  si->write("PWID");
  std::string readbuf = "";
  std::string modelname = "";
  int ntrial=0;
  while(1){
    si->read(readbuf);
    //    std::cout<<"***"<<readbuf<<"***"<<std::endl;
    size_t pwidpos = readbuf.find("PWID");
    if(pwidpos != std::string::npos){
      //parsing the received message
      std::string submessage = readbuf.substr(pwidpos,readbuf.length());
      size_t ncomma = std::count(submessage.begin(), submessage.end(), ',');
      int nexpcomma = 4;
      if(ncomma>=nexpcomma){//Need to adjust the threshold by taking the LVPS model into account...
        std::string tmpmessage = submessage;
        for(int icom=0; icom<nexpcomma; icom++){
          size_t commapos1 = tmpmessage.find(",");
          tmpmessage = tmpmessage.substr(commapos1+1,submessage.length());
          size_t commapos2 = tmpmessage.find(",");
          std::string tmpstr = tmpmessage.substr(0,commapos2);
          //          std::cout<<"hoge: "<<tmpstr<<std::endl;
          if(icom==1){
            modelname = tmpstr;
            break;
          }
        }
      }
    }
    if(modelname!=""){
      si->make_listener();
      usleep(1000000);
      break;
    }
    if(ntrial>10){
      std::cout<<"ERROR: Failed to read texio model name."<<std::endl;
      return "UNKNOWN";
    }
    usleep(500000);
    ntrial++;
  }

  if     (modelname=="PW8-3AQP"   ) this->set_num_available_channel(4);
  else if(modelname=="PW8-3ATP"   ) this->set_num_available_channel(3);
  else if(modelname=="PW8-5ADPS"  ) this->set_num_available_channel(2);
  else if(modelname=="PW16-2ATP"  ) this->set_num_available_channel(3);
  else if(modelname=="PW16-5ADP"  ) this->set_num_available_channel(3);
  else if(modelname=="PW18-1.3AT" ) this->set_num_available_channel(3);
  else if(modelname=="PW18-1.8AQ" ) this->set_num_available_channel(4);
  else if(modelname=="PW18-2ATP"  ) this->set_num_available_channel(3);
  else if(modelname=="PW18-3AD"   ) this->set_num_available_channel(2);
  else if(modelname=="PW18-3ADP"  ) this->set_num_available_channel(2);
  else if(modelname=="PW24-1.5AQ" ) this->set_num_available_channel(4);
  else if(modelname=="PW26-1AT"   ) this->set_num_available_channel(3);
  else if(modelname=="PW36-1.5AD" ) this->set_num_available_channel(2);
  else if(modelname=="PW36-1.5ADP") this->set_num_available_channel(2);
  else{
    this->set_num_available_channel(4);
    std::cout<<"Warning: Unknow model detected. Something wrong may happen. ID="<<modelname<<std::endl;
  }

  return modelname;
};
