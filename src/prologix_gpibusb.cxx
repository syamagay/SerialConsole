#include"prologix_gpibusb.h"
#include"SerialCom.h"

prologix_gpibusb::prologix_gpibusb(){};
prologix_gpibusb::~prologix_gpibusb(){};

int prologix_gpibusb::initialize()
{
  m_address = -1;
  m_initialized = false;

  m_serial = new SerialCom(m_device_name);
  if(not m_serial->is_initialized()) return -1;
  std::string command = "++mode 1\r\n";
#ifdef DEBUG
  std::cout<<this->get_device_name()<<": "<<command<<std::endl;
#endif
  m_serial->write(command);

  m_initialized = true;
  return 0;
};

int prologix_gpibusb::finalize()
{
  delete m_serial;
  return 0;
};

int prologix_gpibusb::set_address(int address)
{
  m_address = address;
  //  std::string command = "++addr "+std::to_string(address)+R"(\r\n)";
  std::string command = "++addr "+std::to_string(address)+"\r\n";
#ifdef DEBUG
  std::cout<<this->get_device_name()<<": "<<command<<std::endl;
#endif
  m_serial->write(command);

  return 0;
}

int prologix_gpibusb::write(std::string command)
{
#ifdef DEBUG
  std::cout<<this->get_device_name()<<": "<<command<<std::endl;
#endif
  m_serial->write(command+"\r\n");
  return 0;
}

std::string prologix_gpibusb::read(std::string &buffer)
{
  m_serial->read(buffer);
  return "";
}

int prologix_gpibusb::make_listener()
{
  std::string command = "++auto 0\r\n";
#ifdef DEBUG
  std::cout<<this->get_device_name()<<": "<<command<<std::endl;
#endif
  m_serial->write(command);
  return 0;
}

int prologix_gpibusb::make_talker()
{
  std::string command = "++auto 1\r\n";
#ifdef DEBUG
  std::cout<<this->get_device_name()<<": "<<command<<std::endl;
#endif
  m_serial->write(command);

  return 0;
}
