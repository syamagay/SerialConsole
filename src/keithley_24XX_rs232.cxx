#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<termios.h>
#include<iostream>
#include<unistd.h>
#include<string.h>
#include"keithley_24XX_rs232.h"

keithley_24XX_rs232::keithley_24XX_rs232(){};
keithley_24XX_rs232::~keithley_24XX_rs232(){};

#define BAUDRATE B9600
//#define BAUDRATE B57600

int keithley_24XX_rs232::initialize()
{
  fd = open(m_device_name.c_str(), O_RDWR | O_NOCTTY );
  if(fd<0){
    perror(m_device_name.c_str());
    return 1;
  }

  tcgetattr(fd, &oldtio); // Preserve current serial port settings.
  bzero(&newtio, sizeof(newtio)); //Clear the new seral port struct.

  //RS232
  newtio.c_cflag = BAUDRATE | CS8 | HUPCL | CLOCAL | CREAD;
  newtio.c_iflag = IXON | ICRNL;
  newtio.c_oflag = OPOST | ONLCR;
  newtio.c_lflag = ISIG | ICANON | IEXTEN | ECHOE | ECHOK | ECHOCTL | ECHOKE;

  newtio.c_cc[VINTR]    = 0; // Ctrl-c
  newtio.c_cc[VQUIT]    = 0; // Ctrl-\
  newtio.c_cc[VERASE]   = 0; // Ctrl-?
  newtio.c_cc[VKILL]    = 0; // Ctrl-u
  newtio.c_cc[VEOF]     = 4; // Ctrl-d
  newtio.c_cc[VTIME]    = 0; // No in-character timer
  newtio.c_cc[VMIN]     = 1; // Blocking until receiving a character
  newtio.c_cc[VSWTC]    = 0; // '\0'
  newtio.c_cc[VSTART]   = 0; // Ctrl-q
  newtio.c_cc[VSTOP]    = 0; // Ctrl-s
  newtio.c_cc[VSUSP]    = 0; // Ctrl-z
  newtio.c_cc[VEOL]     = 0; // '\0'
  newtio.c_cc[VREPRINT] = 0; // Ctrl-r
  newtio.c_cc[VDISCARD] = 0; // Ctrl-u
  newtio.c_cc[VWERASE]  = 0; // Ctrl-w
  newtio.c_cc[VLNEXT]   = 0; // Ctrl-v
  newtio.c_cc[VEOL2]    = 0; // '\0'

  // newtio.c_cc[VINTR]    = 0x03; // Ctrl-c
  // newtio.c_cc[VQUIT]    = 0x1C; // Ctrl-\
  // //  newtio.c_cc[VERASE]   = 0x7F; // Ctrl-?
  // newtio.c_cc[VERASE]   = 0; // Ctrl-?
  // newtio.c_cc[VKILL]    = 0x15; // Ctrl-u
  // newtio.c_cc[VEOF]     = 0x04; // Ctrl-d
  // newtio.c_cc[VTIME]    = 0; // No in-character timer
  // newtio.c_cc[VMIN]     = 1; // Blocking until receiving a character
  // newtio.c_cc[VSWTC]    = 0; // '\0'
  // newtio.c_cc[VSTART]   = 0x11; // Ctrl-q
  // newtio.c_cc[VSTOP]    = 0x13; // Ctrl-s
  // newtio.c_cc[VSUSP]    = 0x1A; // Ctrl-z
  // newtio.c_cc[VEOL]     = 0; // '\0'
  // newtio.c_cc[VREPRINT] = 0x12; // Ctrl-r
  // newtio.c_cc[VDISCARD] = 0x0F; // Ctrl-u
  // newtio.c_cc[VWERASE]  = 0x17; // Ctrl-w
  // newtio.c_cc[VLNEXT]   = 0x16; // Ctrl-v
  // newtio.c_cc[VEOL2]    = 0; // '\0'

  tcflush(fd, TCIFLUSH);
  tcsetattr(fd, TCSANOW, &newtio);

  usleep(20000);

  m_initialized = true;
  return 0;
};

int keithley_24XX_rs232::finalize()
{
  tcsetattr(fd, TCSANOW, &oldtio);
  return 0;
};

int keithley_24XX_rs232::write(std::string command)
{
  std::string tmpcom = "echo \""+command+"\" > "+m_device_name;
  system(tmpcom.c_str());
  usleep(10000);

  return 0;
}

std::string keithley_24XX_rs232::read(std::string &buffer)
{
  char buf[255];
  ::read(fd, buf, 255);
  buffer = buf;

  return "";
}

int keithley_24XX_rs232::make_listener()
{
  return 0;
}

int keithley_24XX_rs232::make_talker()
{
  return 0;
}
