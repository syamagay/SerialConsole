#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<termios.h>
#include<iostream>
#include<unistd.h>
#include<string.h>
#include"texio_if_41rs.h"

char hexint2char(int num){
  if     (num==0) return '0';
  else if(num==1) return '1';
  else if(num==2) return '2';
  else if(num==3) return '3';
  else if(num==4) return '4';
  else if(num==5) return '5';
  else if(num==6) return '6';
  else if(num==7) return '7';
  else if(num==8) return '8';
  else if(num==9) return '9';
  else if(num==10) return 'A';
  else if(num==11) return 'B';
  else if(num==12) return 'C';
  else if(num==13) return 'D';
  else if(num==14) return 'E';
  else if(num==15) return 'F';
  return 0x00;
}

texio_if_41rs::texio_if_41rs(){};
texio_if_41rs::~texio_if_41rs(){};

#define BAUDRATE B9600

int texio_if_41rs::initialize()
{
  fd = open(m_device_name.c_str(), O_RDWR | O_NOCTTY );
  if(fd<0){
    perror(m_device_name.c_str());
    return 1;
  }

  tcgetattr(fd, &oldtio); // Preserve current serial port settings.
  bzero(&newtio, sizeof(newtio)); //Clear the new seral port struct.

  //RS232
  //  newtio.c_cflag = BAUDRATE | CS7 | PARENB | CRTSCTS | CLOCAL | CREAD;
  newtio.c_cflag = BAUDRATE | CS7 | PARENB | CRTSCTS | CLOCAL | CREAD;
  newtio.c_iflag = 0;
  newtio.c_oflag = 0;
  newtio.c_lflag = 0;

  newtio.c_cc[VINTR]    = 0; // Ctrl-c
  newtio.c_cc[VQUIT]    = 0; // Ctrl-\
  newtio.c_cc[VERASE]   = 0; // del
  newtio.c_cc[VKILL]    = 0; // @
  //  newtio.c_cc[VEOF]     = 4; // Ctrl-d
  newtio.c_cc[VEOF]     = 0; // Ctrl-d
  newtio.c_cc[VTIME]    = 0; // No in-character timer
  //  newtio.c_cc[VMIN]     = 1; // Blocking until receiving a character
  newtio.c_cc[VMIN]     = 0; // Blocking until receiving a character
  newtio.c_cc[VSWTC]    = 0; // '\0'
  newtio.c_cc[VSTART]   = 0; // Ctrl-q
  newtio.c_cc[VSTOP]    = 0; // Ctrl-s
  newtio.c_cc[VSUSP]    = 0; // Ctrl-z
  newtio.c_cc[VEOL]     = 0; // '\0'
  newtio.c_cc[VREPRINT] = 0; // Ctrl-r
  newtio.c_cc[VDISCARD] = 0; // Ctrl-u
  newtio.c_cc[VWERASE]  = 0; // Ctrl-w
  newtio.c_cc[VLNEXT]   = 0; // Ctrl-v
  newtio.c_cc[VEOL2]    = 0; // '\0'

  tcflush(fd, TCIFLUSH);
  tcsetattr(fd, TCSANOW, &newtio);

  usleep(2000);

  m_initialized = true;
  return 0;
};

int texio_if_41rs::finalize()
{
  this->write("LC1");
  tcsetattr(fd, TCSANOW, &oldtio);
  return 0;
};

int texio_if_41rs::write(std::string command)
{
  //There should be smarter way to get ENQ and ETX...
  char enq, etx;
  enq='E';enq-=64;
  etx='C';etx-=64;
  std::string str_enq, str_etx;
  str_enq = enq;
  str_etx = etx;

  //Computing block check charactors.
  std::string BC = "";
  char tmpchar = (char)(0x41+m_address-1);//0x41="A" means address=1.
  std::string Addr = "A"; Addr[0] = tmpchar;
  int sumASCII = (int)Addr[0] + 0x3;//System Address and ETX
  for(int i = 0; i<(int)command.size(); ++i){
    char ch = command[i];
    //    std::cout<<ch<<": "<<+ch<<std::endl;
    sumASCII+=(int)ch;
  }
  sumASCII = sumASCII&0x00FF;//Keep eight bits from LSB.
  std::string str_bc1, str_bc2;
  str_bc1 = hexint2char(sumASCII>>4);
  str_bc2 = hexint2char(sumASCII&0x0F);
  BC = str_bc1+str_bc2;
  std::string tmpcom = str_enq+Addr+command+str_etx+BC;
  // std::cout<<sumASCII<<", BC = "<<BC<<std::endl;
  // std::cout<<"Length="<<tmpcom.length()<<", "<<tmpcom<<std::endl;

  buf=const_cast<char*>(tmpcom.c_str());
  ::write(fd, buf, 255);
  usleep(500000);

  return 0;
}

std::string texio_if_41rs::read(std::string &buffer)
{
  char buf[255];
  ::read(fd, buf, 255);
  buffer = buf;

  return "";
}

int texio_if_41rs::make_listener()
{
  char ack;
  ack='F';ack-=64;
  std::string str_ack;
  str_ack = ack;
  std::string tmpcom = str_ack+"A";
  char* buf=const_cast<char*>(tmpcom.c_str());
  ::write(fd, buf, 255);
  usleep(500000);

  return 0;
}

int texio_if_41rs::make_talker()
{
  return 0;
}
