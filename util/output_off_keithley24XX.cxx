#include<iostream>
#include<fstream>
#include<unistd.h>

#include"utility.h"
#include"picojson.h"
#include"keithley2410.h"
#include"prologix_gpibusb.h"
#include"keithley_24XX_rs232.h"

int main(int argc, char* argv[])
{
  //Checking if a config file is given
  if(argc!=2){
    std::cout<<"Usage: "<<argv[0]<<" path/to/config.json"<<std::endl;
    return 1;
  }

  //Retrieve contents of the config file
  std::string configjson = argv[1];
  std::ifstream ifs(configjson.c_str(), std::ios::in);
  if(ifs.fail()){
    std::cout<<"Error: Failed to read "<<configjson<<std::endl;
    return 1;
  }
  const std::string json((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
  ifs.close();

  //Parsing the config file
  picojson::value v;
  const std::string err = picojson::parse(v, json);
  if (err.empty() == false) {
    std::cout<<"Something wrong in "<<configjson<<std::endl;
    std::cout<<err<<std::endl;
    return 1;
  }
  picojson::object& obj = v.get<picojson::object>();

  //Checking if necessary values are set in the config
  if(obj["devicefile"].is<picojson::null>()){
    std::cout<<"\"devicefile\": is not set in "<<configjson<<std::endl;;
    return 1;
  }
  if(obj["address"].is<picojson::null>()){
    std::cout<<"\"address\": is not set in "<<configjson<<std::endl;;
    return 1;
  }
  if(obj["interfacetype"].is<picojson::null>()){
    std::cout<<"\"interfacetype\": is not set in "<<configjson<<std::endl;;
    return 1;
  }

  //Showing contents of the config file
  std::cout<<"Control based on settings below ============="<<std::endl;
  std::cout<<"devicefile: "<<obj["devicefile"].get<std::string>()<<std::endl;
  std::cout<<"address: "<<obj["address"].get<double>()<<std::endl;
  std::cout<<"interfacetype: "<<obj["interfacetype"].get<std::string>()<<std::endl;
  if(not obj["devicetype"].is<picojson::null>()){
    std::cout<<"devicetype: "<<obj["devicetype"].get<std::string>()<<std::endl;
  }
  // picojson::array& array = obj["channels"].get<picojson::array>();
  // for(picojson::array::iterator it = array.begin(); it != array.end(); it++){
  //   picojson::object& tmp = it->get<picojson::object>();
  //   std::cout<<"Output "<<tmp["channel"].get<std::string>()<<": "<<
  //     ((not tmp["voltage"].is<picojson::null>())?std::to_string(tmp["voltage"].get<double>()).substr(0,5)+"[V]":"null")<<", "<<
  //     ((not tmp["current"].is<picojson::null>())?std::to_string(tmp["current"].get<double>()).substr(0,5)+"[A]":"null")<<std::endl;
  // }

  std::string device_name = obj["devicefile"].get<std::string>();

  //Making serial interface for devices
  serial_interface* serial_interface;
  if(obj["interfacetype"].get<std::string>()=="RS232"){
    serial_interface = new keithley_24XX_rs232();
  }else if(obj["interfacetype"].get<std::string>()=="GPIB"){
    serial_interface = new prologix_gpibusb();
  }else{
    std::cout<<"[ERROR] Interface type should be \"RS232\" or \"GPIB\"."<<std::endl;
    return -1;
  }

  serial_interface->set_device_name(device_name);
  serial_interface->initialize();
  if(not serial_interface->is_initialized()){
    std::cout<<"[ERROR] failed to create an instance of the serial interface for "<<device_name<<"."<<std::endl;
    return -1;
  }

  //Making power supply instance
  power_supply* power_supply = new keithley2410(obj["address"].get<double>());
  if(power_supply->is_off(serial_interface)){
    std::cout<<"Output is already disabled."<<std::endl;
    return -1;
  }

  //Reading voltage and current
  double voltage(0.);
  voltage = power_supply->read_voltage(serial_interface);
  power_supply->set_voltage(0.);
  if(voltage<1.) power_supply->config_voltage(serial_interface);
  else           power_supply->voltage_sweep (serial_interface);
  power_supply->power_off(serial_interface);

  std::cout<<"Output disabled."<<std::endl;

  // //Making devices to control
  // double keithley2410_1_compliance = 10.E-6; //should be increased for irrad. sensors
  // power_supply* keithley2410_1 = new keithley2410(keithley2410_1_address);
  // if(keithley2410_1->is_on(serial_interface)){
  //   std::cout<<"Output is already enabled. Doing nothing..."<<std::endl;
  //   return 1;
  // }
  // keithley2410_1->set_voltage(0.0); //initial voltage
  // keithley2410_1->set_compliance(keithley2410_1_compliance);
  // keithley2410_1->configure(serial_interface); //Configuring device
  // keithley2410_1->power_on(serial_interface); //Output ON with initial voltage

  // //Reading voltage and current
  // double voltage(0.), current(0.);
  // keithley2410_1->read_voltage_and_current(serial_interface, voltage, current);

  // //Output information
  // std::string out_format = get_out_format(voltage, current);
  // std::cout<<out_format<<std::endl;

  delete serial_interface;
  delete power_supply;

  return 0;
}
