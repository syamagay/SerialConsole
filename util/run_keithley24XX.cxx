#include<iostream>
#include<fstream>
#include<chrono>
#include<ctime>
#include<thread>
#include<fcntl.h>
#include<termios.h>
#include<unistd.h>

#include"utility.h"
#include"picojson.h"
#include"keithley2410.h"
#include"prologix_gpibusb.h"

int nNo=99;

void ReadIV(power_supply *ps, serial_interface *si)
{
  if(not ps->is_on(si)){
    printf("Voltage and current reading can be performed only when the output is enabled. Doing nothing...\n\n");
    return;
  }

  clearStdin();
  int trial=0;
  printf("==============================================\n");
  printf("Hit any key to return to the prompt.\n");
  printf("==============================================\n");
  while(1){
    //Reading voltage and current
    double voltage(0.), current(0.);
    ps->read_voltage_and_current(si, voltage, current);
    //Preparing output file
    std::ofstream ofile_keithley_2410_1(ps->get_ofile_name(), std::ofstream::app);
    std::string out_format = get_out_format(voltage, current);
    ofile_keithley_2410_1<<out_format<<std::endl;
    std::cout<<out_format<<std::endl;

    if(kbhit()) break;
    trial++;
    sleep(1);
  }
  clearStdin();

  return;
}

void SelectFunction(){
  printf("====================================================\n");
  printf("Please specify what you want to do... \n");
  printf(" 0: Initializing Keithley2410 \n");
  printf(" 1: Reset Keithley2410 \n");
  printf(" 2: Configure HV value\n");
  printf(" 3: Sweep HV value to the target\n");
  printf(" 4: Output ON\n");
  printf(" 5: Output OFF\n");
  printf(" 6: Read voltage/current\n");
  printf("100: Change #sweep steps\n");
  printf("101: Change sweep interval\n");
  //  printf(" 7: Read voltage/current for all working devices\n");
  //  printf("20: IV measurement\n");
  printf("99: exit \n");

  while(1){
    nNo=999;
    printf("No = ");
    scanf("%d",&nNo);
    if(nNo==999) clearStdin();
    printf("\n");
    if(nNo >= 0) break;
  }

  return;
}

int CallFunction(power_supply *ps, serial_interface *si){
  int id = 999;
  int addr = 0;
  float HV = 10000;
  int para = -1;
  switch(nNo){
  case 0:
    printf("=== Configuare Keithley2410.\n");
    ps->configure(si);
    break;
  case 1:
    printf("=== Reset Keithley2410.\n");
    ps->reset(si);
    break;
  case 2:
    printf("=== Set HV value.\n");
    printf("Hit desired voltage [V]: ");
    scanf("%f",&HV);
    if(HV==10000){ //Fail-safe for someone who hit non-numeric value
      printf("Hit a number...");
      clearStdin();
      break;
    }
    ps->set_voltage(HV);
    ps->config_voltage(si);
    break;
  case 3:
    if(not ps->is_on(si)){
      printf("Voltage sweep is only available with output ON. Doing nothing...\n\n");
      break;
    }
    printf("=== Voltage sweep to the target value.\n");
    printf("Hit desired voltage [V]: ");
    scanf("%f",&HV);
    if(HV==10000){ //Fail-safe for someone who hit non-numeric value
      printf("Hit a number...");
      clearStdin();
      break;
    }
    std::cout<<"Configuring Voltage to "<<HV<<" [V]"<<std::endl;
    ps->set_voltage(HV);
    ps->voltage_sweep(si);
    break;
  case 4:
    printf("=== Set output ON.\n");
    ps->power_on(si); //Output ON with initial voltage
    break;
  case 5:
    printf("=== Set output OFF.\n");
    ps->power_off(si); //Output ON with initial voltage
    break;
  case 6:
    printf("=== Read voltage and current.\n");
    ReadIV(ps,si);
    break;

  case 100:
    printf("=== Change #sweep steps.\n");
    printf("Hit desired steps: ");
    scanf("%d",&para);
    if(para<0){ //Fail-safe for someone who hit non-numeric value
      printf("Invalid number...");
      clearStdin();
      break;
    }
    std::cout<<"Configuring #sweep steps to "<<para<<std::endl;
    ps->set_sweep_steps(para);
    break;
  case 101:
    printf("=== Change #sweep sleep time.\n");
    printf("Hit desired sleep time [ms]: ");
    scanf("%d",&para);
    if(para<0){ //Fail-safe for someone who hit non-numeric value
      printf("Invalid number...");
      clearStdin();
      break;
    }
    std::cout<<"Configuring sweep sleep time to "<<para<<std::endl;
    ps->set_sweep_sleep_in_ms(para);
    break;
  // case 7:
  //   printf("=== Monitoring voltage and current for all working keithleys.\n");
  //   ReadIVAll();
  //   break;
  // case 20:
  //   printf("=== IV measurement.\n");
  //   printf("Hit address:");
  //   scanf("%d",&addr);
  //   if(addr<1 || 15<=addr){
  //     printf("Please specify address in range (2-15).");
  //   }else{
  //     char tmpaddr[5];
  //     sprintf(tmpaddr, "%d", addr);
  //     IVMeasure(tmpaddr);
  //   }
  //   break;
  case 99:
    return 1;
  default:
    printf("not defined. doing nothing...\n");
    break;
  }
  printf("\n");

  return 0;
}

int main(int argc, char* argv[])
{
  std::cout<<"Started PowerSupply control application."<<std::endl;

  //Checking if a config file is given
  if(argc!=2){
    std::cout<<"Usage: "<<argv[0]<<" path/to/config.json"<<std::endl;
    return 1;
  }

  //Retrieve contents of the config file
  std::string configjson = argv[1];
  std::ifstream ifs(configjson.c_str(), std::ios::in);
  if(ifs.fail()){
    std::cout<<"Error: Failed to read "<<configjson<<std::endl;
    return 1;
  }
  const std::string json((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
  ifs.close();

  //Parsing the config file
  picojson::value v;
  const std::string err = picojson::parse(v, json);
  if (err.empty() == false) {
    std::cout<<"Something wrong in "<<configjson<<std::endl;
    std::cout<<err<<std::endl;
    return 1;
  }
  picojson::object& obj = v.get<picojson::object>();

  //Checking if necessary values are set in the config
  if(obj["devicefile"].is<picojson::null>()){
    std::cout<<"\"devicefile\": is not set in "<<configjson<<std::endl;;
    return 1;
  }
  if(obj["address"].is<picojson::null>()){
    std::cout<<"\"address\": is not set in "<<configjson<<std::endl;;
    return 1;
  }

  //Showing contents of the config file
  std::cout<<"Control based on settings below ============="<<std::endl;
  std::cout<<"devicefile: "<<obj["devicefile"].get<std::string>()<<std::endl;
  std::cout<<"address: "<<obj["address"].get<double>()<<std::endl;
  if(not obj["devicetype"].is<picojson::null>()){
    std::cout<<"devicetype: "<<obj["devicetype"].get<std::string>()<<std::endl;
  }
  picojson::array& array = obj["channels"].get<picojson::array>();
  for(picojson::array::iterator it = array.begin(); it != array.end(); it++){
    picojson::object& tmp = it->get<picojson::object>();
    std::cout<<"Output "<<tmp["channel"].get<std::string>()<<": "<<
      ((not tmp["voltage"].is<picojson::null>())?std::to_string(tmp["voltage"].get<double>()).substr(0,5)+"[V]":"null")<<", "<<
      ((not tmp["current"].is<picojson::null>())?std::to_string(tmp["current"].get<double>()).substr(0,5)+"[A]":"null")<<std::endl;
  }

  std::string device_name = obj["devicefile"].get<std::string>();

  //Making serial interface for devices
  serial_interface* serial_interface = new prologix_gpibusb();
  serial_interface->set_device_name(device_name);
  serial_interface->initialize();
  if(not serial_interface->is_initialized()){
    std::cout<<"[ERROR] failed to create an instance of the serial interface for "<<device_name<<"."<<std::endl;
    return -1;
  }

  //Making power supply instance
  power_supply* power_supply = new keithley2410(obj["address"].get<double>());
  power_supply->set_voltage(0.0); //initial voltage
  power_supply->set_sweep_steps(obj["sweep_steps"].get<double>());

  for(picojson::array::iterator it = array.begin(); it != array.end(); it++){
    picojson::object& tmp = it->get<picojson::object>();
    int channel = -1;
    if     (tmp["channel"].get<std::string>()=="A") channel = 0;
    else if(tmp["channel"].get<std::string>()=="B") channel = 1;
    else if(tmp["channel"].get<std::string>()=="C") channel = 2;
    else if(tmp["channel"].get<std::string>()=="D") channel = 3;
    else std::cout<<"Warning: Channel should be in range A-D ("<<tmp["channel"].get<std::string>()<<" was given)."<<std::endl;
    if(channel!=-1){
      power_supply->set_enable(true, channel);
      if(tmp["current"].is<picojson::null>()){
        power_supply->set_compliance(0., channel);
        power_supply->set_enable(false, channel);
      }
      else power_supply->set_compliance(tmp["current"].get<double>(),channel);
    }
  }

  while(1){
    SelectFunction();
    int ret = CallFunction(power_supply, serial_interface);
    if(ret!=0) break;
  }

  delete serial_interface;
  delete power_supply;

  std::cout<<"Closing application..."<<std::endl;

  return 0;
}
