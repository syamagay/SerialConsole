#include<iostream>
#include<fstream>
#include<unistd.h>

#include"utility.h"
#include"picojson.h"
#include"texio_if_41rs.h"
#include"texio_pw_a_series.h"

int main(int argc, char* argv[])
{
  //Checking if a config file is given
  if(argc!=2){
    std::cout<<"Usage: "<<argv[0]<<" path/to/config.json"<<std::endl;
    return 1;
  }

  //Retrieve contents of the config file
  std::string configjson = argv[1];
  std::ifstream ifs(configjson.c_str(), std::ios::in);
  if(ifs.fail()){
    std::cout<<"Error: Failed to read "<<configjson<<std::endl;
    return 1;
  }
  const std::string json((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
  ifs.close();

  //Parsing the config file
  picojson::value v;
  const std::string err = picojson::parse(v, json);
  if (err.empty() == false) {
    std::cout<<"Something wrong in "<<configjson<<std::endl;
    std::cout<<err<<std::endl;
    return 1;
  }
  picojson::object& obj = v.get<picojson::object>();

  //Checking if necessary values are set in the config
  if(obj["address"].is<picojson::null>()){
    std::cout<<"\"address\": is not set in "<<configjson<<std::endl;;
    return 1;
  }

  //Showing contents of the config file
  std::cout<<"Control based on settings below ============="<<std::endl;
  std::cout<<"devicefile: "<<obj["devicefile"].get<std::string>()<<std::endl;
  if(not obj["devicetype"].is<picojson::null>()){
    std::cout<<"devicetype: "<<obj["devicetype"].get<std::string>()<<std::endl;
  }
  if(not obj["address"].is<picojson::null>()){
    std::cout<<"address: "<<obj["address"].get<double>()<<std::endl;
  }
  picojson::array& array = obj["channels"].get<picojson::array>();
  for(picojson::array::iterator it = array.begin(); it != array.end(); it++){
    picojson::object& tmp = it->get<picojson::object>();
    std::cout<<"Output "<<tmp["channel"].get<std::string>()<<": "<<
      ((not tmp["voltage"].is<picojson::null>())?std::to_string(tmp["voltage"].get<double>()).substr(0,5)+"[V]":"null")<<", "<<
      ((not tmp["current"].is<picojson::null>())?std::to_string(tmp["current"].get<double>()).substr(0,5)+"[A]":"null")<<std::endl;
  }

  std::string device_name = obj["devicefile"].get<std::string>();

  //Making serial interface for devices
  serial_interface* serial_interface = new texio_if_41rs();
  serial_interface->set_device_name(device_name);
  serial_interface->initialize();
  if(not serial_interface->is_initialized()){
    std::cout<<"[ERROR] failed to create an instance of the serial interface for "<<device_name<<"."<<std::endl;
    return -1;
  }

  //Making power supply instance
  power_supply* power_supply = new texio_pw_a_series(obj["address"].get<double>());
  power_supply->initialize(serial_interface);

  for(picojson::array::iterator it = array.begin(); it != array.end(); it++){
    picojson::object& tmp = it->get<picojson::object>();
    int channel = -1;
    if     (tmp["channel"].get<std::string>()=="A") channel = 0;
    else if(tmp["channel"].get<std::string>()=="B") channel = 1;
    else if(tmp["channel"].get<std::string>()=="C") channel = 2;
    else if(tmp["channel"].get<std::string>()=="D") channel = 3;
    else std::cout<<"Warning: Channel should be in range A-D ("<<tmp["channel"].get<std::string>()<<" was given)."<<std::endl;
    if(channel!=-1){
      power_supply->set_enable(true, channel);
      if(tmp["voltage"].is<picojson::null>()){
        power_supply->set_voltage(0., channel);
        power_supply->set_enable(false, channel);
      }
      else power_supply->set_voltage(tmp["voltage"].get<double>(),channel);
      if(tmp["current"].is<picojson::null>()){
        power_supply->set_compliance(0., channel);
        power_supply->set_enable(false, channel);
      }
      else power_supply->set_compliance(tmp["current"].get<double>(),channel);
    }
  }

  //Trying to read IV
  std::string readbuf = "";
  serial_interface->write("ST4");
  int ntrial=0;
  const int nchannel=power_supply->get_num_available_channel();
  double voltage[nchannel];
  double current[nchannel];
  std::string status[nchannel];
  while(1){
    serial_interface->read(readbuf);
    //    std::cout<<"***"<<readbuf<<"***"<<std::endl;
    size_t ms4pos = readbuf.find("MS4");
    if(ms4pos != std::string::npos){
      //parsing the received message
      std::string submessage = readbuf.substr(ms4pos,readbuf.length());
      size_t ncomma = std::count(submessage.begin(), submessage.end(), ',');
      int nexpcomma = 2*nchannel+2;
      if(ncomma>=nexpcomma){//Need to adjust the threshold by taking the LVPS model into account...
        std::string tmpmessage = submessage;
        for(int icom=0; icom<nexpcomma; icom++){
          size_t commapos1 = tmpmessage.find(",");
          tmpmessage = tmpmessage.substr(commapos1+1,submessage.length());
          size_t commapos2 = tmpmessage.find(",");
          std::string numstr = tmpmessage.substr(0,commapos2);
          if(icom!=0 && icom!=nexpcomma-1){
            if(icom%2==1) voltage[(icom-1)/2] = std::stod(numstr);
            else          current[(icom-1)/2] = std::stod(numstr);
          }
          if(icom==nexpcomma-1){
            for(int jcnt=0; jcnt<nchannel; jcnt++){
              status[jcnt] = tmpmessage.substr(jcnt,1)=="0"?"CV":"CC";
            }
          }
        }
        serial_interface->make_listener();
        break;
      }
    }
    if(ntrial>10){
      std::cout<<"ERROR: Failed to read voltage and current."<<std::endl;
      serial_interface->finalize();
      delete serial_interface;//Needed to be the original SIO state.
      return 1;
    }
    usleep(500000);
    ntrial++;
  }

  //Output information
  for(int icnt=0; icnt<nchannel; icnt++){
    std::string out_format = get_out_format(voltage[icnt], current[icnt]);
    std::string str_chan;
    if     (icnt==0) str_chan = "A";
    else if(icnt==1) str_chan = "B";
    else if(icnt==2) str_chan = "C";
    else if(icnt==3) str_chan = "D";
    std::cout<<str_chan<<": "<<out_format<<", "<<status[icnt]<<std::endl;
  }

  serial_interface->finalize();

  delete serial_interface;
  delete power_supply;

  sleep(1);//Need to wait for a while...
  return 0;
}
